---
title: Map of the site
date: 2020-01-01
---
![Map_of_site](/images/Map_of_site.png)

- Privately owned and maintained.
- Open to public from 1 November 2020.
- 17 Acres of pasture land to be converted to woodland and wild flower meadow with support from the Woodland Trust and local people.
- Tree planting in December 2020.
- Establishing wildflower meadow Autumn 2020.
