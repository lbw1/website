---
title: About us
subtitle: Who we are
comments: false
---
![About Us](/images/aboutus.jpg)
Like many people across the globe and in our local community, my concern about climate change and the environmental legacy we are leaving to future generations has been growing over the last few years. Do I really want to be part of the generation that destroyed our beautiful planet?

However, having concerns about a global problem like climate change is necessary but not sufficient to do anything about it. Fortunately, I am in a position with your help and the support from my wife! to make a small contribution. Last year I purchased 17 acres of pasture land south of Braceborough which has been used for sheep grazing for at least the last 20 years. My vision for this land is to establish and maintain a natural native woodland to support wildlife and the environment for the enjoyment of all.

I now need your help - I have obtained a grant from the Woodland Trust to plant 2,000 trees this winter on the lower of the two fields. I will therefore need a large number of volunteers to help plant these trees (tentatively set for the weekend of the 5/6 December 2020). If you are willing to help with this tree planting please let me know at richard.boulderstone@gmail.com so that we can provide all the logistics.

I apologise for any disruption that this event may cause - I am hopeful, with your help, that the myriad of potential problems (CoVid, weather, parking, trees arriving on time, enough volunteers, tools, etc.) will be overcome and that we will create a lasting resource for our local community.

